﻿using RabbitMQ.Wrapper.ProducerConsumer;
using System;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            Sender sender = new Sender("exchange_topic", "pong");
            sender.Send("ping");//start messaging by sending to pong  exchange
            sender.Dispose();
            Reciver resiver = new Reciver("ping_queue", "exchange_topic", "ping");
            resiver.ListenQueue();
            Console.Read();
            resiver.Dispose();
            Console.Read();
        }
    }

}
