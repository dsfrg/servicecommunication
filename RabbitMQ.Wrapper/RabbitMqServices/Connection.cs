﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.RabbitMqServices
{
    public static  class Connection
    {
        public static IConnectionFactory GetConnetionFactory()
        {
            IConnectionFactory factory = new ConnectionFactory()
            {
                HostName = ConnectionFactorySettings.HostName,
                Port = ConnectionFactorySettings.Host,
                UserName = ConnectionFactorySettings.UserName,
                Password = ConnectionFactorySettings.Password,
                VirtualHost = ConnectionFactorySettings.VirtualHost,
                ContinuationTimeout = ConnectionFactorySettings.ContinuationTimeout,
            };
            return factory;
        }
    }
}
