﻿using RabbitMQ.Wrapper.ProducerConsumer;
using System;
using System.Timers;


namespace RabbitMQ.Wrapper
{
    public class TimerService: IDisposable
    {
        public delegate void SendMessageDelegate();
        public readonly Timer timer;
        private readonly SendMessageDelegate sendMessageDelegate;
        private readonly Sender _sender;
        public string Message { get; set; }

        public TimerService(Sender sender_, string message)
        {
            _sender = sender_;
            Message = message;
            sendMessageDelegate = new SendMessageDelegate(SendToQueue);
            timer = new Timer();
            
        }
        public void StartReadout()
        {
            timer.Interval = 2500;
            timer.Start();
            timer.Elapsed += (sender, e) => timer_Elapsed(sendMessageDelegate);
        }

        public void timer_Elapsed(SendMessageDelegate p)
        {
            p.Invoke();
            Console.WriteLine(" [x] Sent {0} at {1}", Message, DateTime.Now);
            Dispose();
        }
        public void SendToQueue()
        {
            _sender.Send(Message);
        }

        public void Dispose()
        {
            timer.Stop();
            timer.Dispose();
        }
    }
}
