﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.RabbitMqServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.ProducerConsumer
{
    public class Reciver : IDisposable
    {
        private readonly IModel _model;
        private readonly EventingBasicConsumer _consumer;
        public string QueueName { get; set; }
        public string ExchangeName { get; set; }
        public string RoutingKey { get; set; }
        public Reciver(string queueName, string exchangeName, string routingKey)
        {
            _model = Connection.GetConnetionFactory().CreateConnection().CreateModel();
            _consumer = new EventingBasicConsumer(_model);
            QueueName = queueName;
            ExchangeName = exchangeName;
            RoutingKey = routingKey;
            Connect();
        }


        public void Connect()
        { 
            _model.QueueDeclare(QueueName, true, false, false);
            _model.QueueBind(QueueName, ExchangeName, RoutingKey);
        }


        public void ListenQueue()
        {
           
            _consumer.Received += (model, ea) =>
            {
                var message = GetMessageFromBasicDeliverEventArgs(ea);
                SendToOppositeQueue(message).StartReadout();
                SetAcknowledge(ea.DeliveryTag, true);
            };
            _model.BasicConsume(queue: QueueName,
                                 autoAck: false,
                                 consumer: _consumer);
        }
        
        public TimerService SendToOppositeQueue(string msg)
        {
            Sender sender = new Sender(ExchangeName, RoutingKey == "pong" ? "ping" : "pong");
            TimerService timerService = new TimerService(sender, msg == "pong" ? "ping" : "pong");
            return timerService;
        }

        public void SetAcknowledge(ulong deliveryTag, bool processed)
        {
            if (processed)
            {
                _model.BasicAck(deliveryTag, multiple: false);
            }
            else
            {
                _model.BasicNack(deliveryTag, multiple: false, requeue: true);
            }
        }

        public string GetMessageFromBasicDeliverEventArgs(BasicDeliverEventArgs argc)
        {
            var body = argc.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine(" [x] Received {0} at {1}", message, DateTime.Now);
            // Console.WriteLine($"{message} recived and will be sended to othet queue after 2.5s");
            return message;
        }
        public void Dispose()
        {
            _model?.Close();
        }
    }
}
