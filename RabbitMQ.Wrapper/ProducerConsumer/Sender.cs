﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.RabbitMqServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.ProducerConsumer
{
    public class Sender : IDisposable
    {
        private readonly IModel _model;
        private readonly IBasicProperties _properties;
        public readonly PublicationAddress PublicationAddress;

        public Sender(string exchangeName, string routingKey)
        {
            _model = Connection.GetConnetionFactory().CreateConnection().CreateModel();
            _properties = _model.CreateBasicProperties();
            _properties.Persistent = true;

            PublicationAddress = new PublicationAddress(
                    "topic",
                    exchangeName: exchangeName,
                    routingKey: routingKey);
        }


        public void Send(string message, string type = null)
        {
            if (!String.IsNullOrEmpty(type))
            {
                _properties.Type = type;
            }
            _model.ExchangeDeclare(PublicationAddress.ExchangeName, "topic");
            var body = Encoding.UTF8.GetBytes(message);
            _model.BasicPublish(PublicationAddress,
                _properties, body);
        }

        public void SendMessageToQueue(Type type, string message)
        {
            Send(message, type.AssemblyQualifiedName);
        }
        public void Dispose()
        {
            _model?.Close();
        }
    }
}
