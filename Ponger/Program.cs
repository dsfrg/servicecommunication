﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.ProducerConsumer;
using System;
using System.Text;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            Reciver resiver = new Reciver("pong_queue", "exchange_topic", "pong");
            resiver.ListenQueue();
            Console.Read();
            resiver.Dispose();
            Console.Read();
        }
    }
}


